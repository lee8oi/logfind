#include <stdio.h>
#include <stdlib.h>
#include "dbg.h"
#include "file.h"

StringFile *sfile_new(const char *filename)
{
    StringFile *sf;
    sf = malloc(sizeof(StringFile));
    check_mem(sf);
    sf->size = 0;
    check(sfile_load(filename, sf) == 0, "Failed to load StringFile.");
    
    return sf;
error:
    return NULL;
}

int sfile_open(const char *filename, StringFile *sf)
{
    int rc = sscanf(filename, "%m[\001-\377]", &sf->filename);
    check(rc == 1, "Failed to scan filename.");

    sf->file = fopen(sf->filename, "r");
    check(sf->file, "Failed to read file.");

    return 0;
error:
    return -1;
}

int sfile_scan(StringFile *sf)
{
    int rc = fscanf(sf->file, "%m[\001-\377]", &sf->string);
    check(rc > 0 && errno == 0, "Failed to scan file.");

    sf->size = strlen(sf->string);
    debug("StringFile scan: %s size: %d", sf->filename, sf->size);

    return 0;
error:
    return 1;
}

int sfile_load(const char *filename, StringFile *sf) {
    int rc = sfile_open(filename, sf); // open file
    check(rc == 0, "Failed to load file.");

    rc = sfile_scan(sf); // scan file to string
    check(rc == 0, "Failed to scan file.");

    return 0;
error:
    return -1;
}

void sfile_free(StringFile *sf)
{
    fclose(sf->file);
    free(sf->filename);
    free(sf->string);    
    free(sf);
}
