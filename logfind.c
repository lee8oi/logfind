#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glob.h>
#include <ctype.h>
#include "dbg.h"
#include "stack.h"
#include "file.h"

int OR_FLAG = 0;
char *AllowedSrc = "~/.logfind";


Stack *scan_args(int argc, char *argv[])
{
    Stack *stk = stack_new();
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 'o':
                    debug("OR switch detected\n");
                    OR_FLAG = 1;
                    break;
                default:
                    printf("Unknown flag '-%c'\n", argv[i][1]);
            }
        } else {
            debug("Search term: %s", argv[i]);
            stack_append(stk, argv[i]);
        }
    }
    return stk;
}

Stack *string_split(char *s)
{
    char *substring;
    int count = 0, rc = 1;

    Stack *stk = stack_new();
    substring = s;

    while (rc != EOF) {
        char *line;
        rc = sscanf(substring, "%m[^\n]\001-\377]", &line);
        if (line && rc > 0) {
            count++;
            substring = substring + (strlen(line) + 1);
            stack_append(stk, line);
            free(line);
        } else {
            break;
        }
    }

    return stk;
}

int search(StringFile *sf, Stack *terms)
{
    Stack *matched = stack_new();
    Stack *matched_terms = stack_new();
    char *sub;
    for (int i = 0; i < terms->size; i++) {
        sub = strstr(sf->string, terms->strings[i]);
        if (sub) {
            // 'rewind' to start of line
            while (sub != sf->string && *(sub - 1) && *(sub - 1) != '\n') {
                sub--;
            }

            char *line;
            int rc = sscanf(sub, "%m[^\n]\001-\377]", &line);
            check(rc == 1, "Failed to scan line.");

            stack_append(matched, line);
            stack_append(matched_terms, terms->strings[i]);

            debug("%s\n", line);
            free(line);
            if (OR_FLAG) break;
        }
    }

    if (matched->size > 0) {
       if (OR_FLAG && matched->size > 0) {
            printf("\033[0;32m%s\033[0m '%s':\n ", sf->filename,
                        matched_terms->strings[0]);
            printf("%s\n", matched->strings[0]);
            printf("\033[0;33mMATCH:\033[0m %s\n", sf->filename);
       }
       if (matched->size == terms->size) { // all terms matched
            for (int i = 0; i < matched->size; i++ ) {
                printf("\033[0;32m%s\033[0m '%s':\n ", sf->filename,
                        matched_terms->strings[i]);
                printf("%s\n", matched->strings[i]);
            }
            printf("\033[0;33mMATCH:\033[0m %s\n", sf->filename);
        }
    }

    stack_free(matched);
    stack_free(matched_terms);
    return 0;
error:
    stack_free(matched);
    stack_free(matched_terms);
    return -1;
}
int errfunc (const char *filename, int errcode)
{
    check(errcode, "Glob error");
    return 0;
error:
    return -1;
}

int main(int argc, char *argv[])
{
    // glob the AllowedSrc path into a useable one
    glob_t g; int rc = glob(AllowedSrc, GLOB_NOSORT|GLOB_TILDE, errfunc, &g);
    if (rc != 0) {
        log_err("Failed to glob %s\n", AllowedSrc);
        exit(1);
    }
    debug("%s", g.gl_pathv[0]);
    
    Stack *args = scan_args(argc, argv);

    // load the 'allowed' file list (~/.logfind)
    StringFile *allowed_file = sfile_new(g.gl_pathv[0]);
    Stack *allowed_list = string_split(allowed_file->string);

    globfree(&g);
    
    for (int i = 0; i < allowed_list->size; i++) {
        glob_t g;
        int rc = glob(allowed_list->strings[i], GLOB_NOSORT|GLOB_TILDE, errfunc, &g);
        check(rc == 0, "Failed to glob %s\n", allowed_list->strings[i]);

        for (int x = 0; x < g.gl_pathc; x++) {
            debug("FILE: %s\n", g.gl_pathv[x]);
            
            StringFile *current = sfile_new(g.gl_pathv[x]);

            int matches = search(current, args);
            check(matches >= 0, "Failed to search for terms.");
            
            sfile_free(current);
        }
        globfree(&g);
    }

    stack_free(allowed_list);
    sfile_free(allowed_file);
    stack_free(args);
    return 0;
error:
    globfree(&g);
    stack_free(allowed_list);
    sfile_free(allowed_file);
    stack_free(args);
    return 1;

}