#ifndef _file_h
#define _file_h
#include <stdio.h>

typedef struct StringFile {
    char *filename;
    char *string;
    int size;
    FILE *file;
} StringFile;

StringFile *sfile_new(const char *filename);

int sfile_open(const char *filename, StringFile *sf);

int sfile_scan(StringFile *sf);

int sfile_load(const char *filename, StringFile *sf);

void sfile_free(StringFile *sf);

#endif