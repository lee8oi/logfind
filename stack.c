#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dbg.h"
#include "stack.h"

Stack *stack_new()
{
    Stack *s;
    s = malloc(sizeof(Stack));
    s->size = 0;
    s->strings = calloc(1, sizeof(char **));
    return s;
}

int stack_append(Stack *s, const char *str)
{
    s->strings = realloc(s->strings, sizeof(char **) * (s->size + 1));
    check_mem(s->strings);

    int rc = sscanf(str, "%m[\001-\377]", &s->strings[s->size]);
    check(rc == 1, "Failed to scan string.");
    s->size++;
    
    debug("Appended: length: %d string: %s", (int)strlen(str), str);

    return 0;
error:
    return -1;
}

void stack_print(Stack *stk)
{
    printf("Stack size: %d\n", stk->size);
    for (int i = 0; i < stk->size; i ++) {
        printf("%d> %s\n", i, stk->strings[i]);
    }
}

void stack_free(Stack *s)
{
    for (int i = 0; i < s->size; i++) {
        if (s->strings[i]) free(s->strings[i]);
    }
    free(s->strings);
    free(s);
}

