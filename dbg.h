#ifndef __dbg_h__
#define __dbg_h__

#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) eprintf("[DEBUG]\t(%s:%d:%s) " M,\
        __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define eprintf(M, ...) fprintf(stderr, M "\n", ##__VA_ARGS__)

/**
 * Logging
 */

#define log_err(M, ...) eprintf(\
    "[ERROR]\t(%s:%d:%s) (errno: %s) " M, __FILE__, __LINE__, __func__,\
    clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...) eprintf(\
    "[WARN]\t(%s:%d:%s) (errno: %s) " M, __FILE__, __LINE__, __func__,\
    clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) eprintf(\
    "[INFO]\t(%s:%d:%s) " M, __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#define log_time() fprintf(stdout, "TIME: %s\n", __TIME__)

#define log_date() fprintf(stdout, "DATE: %s\n", __DATE__)

/**
 * Error Checking
 */

#define check(A, M, ...) if (!(A)) {\
        log_err(M, ##__VA_ARGS__); errno = 0; goto error; }

#define sentinel(M, ...) { log_err(M, ##__VA_ARGS__);\
        errno = 0; goto error; }

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(A, M, ...) if (!(A)) { debug(M, ##__VA_ARGS__);\
        errno = 0; goto error; }

/**
 * Miscellaneous
 */

#define stringify(M) #M

#define macro_stringify(M) stringify(M)

#define sif(A) (A) ? "true" : "false"

#endif