CFLAGS=-Wall -g

logfind: logfind.c stack.o file.o
	cc $(CFLAGS) -DNDEBUG logfind.c stack.o file.o -o logfind

file.o: file.c file.h
	cc $(CFLAGS) -DNDEBUG -c file.c

stack.o: stack.c stack.h
	cc $(CFLAGS) -DNDEBUG -c stack.c

check:
	cppcheck *.c *.h

memcheck:
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./logfind ake .h log

test:
	./logfind ake .h log && echo
	./logfind -o ake .h log

clean:
	rm -f logfind vgcore.* *.o