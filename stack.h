#ifndef _stack_h
#define _stack_h

typedef struct Stack {
    int size;
    char **strings;
} Stack;

Stack *stack_new();

int stack_append(Stack *s, const char *str);

void stack_print(Stack *stk);

void stack_free(Stack *s);

#endif